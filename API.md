# 接口文档
## api接口说明
 本接口文档api文档返回格式请求成功统一为  
 **在没有特殊说明的情况下，该文档如果下面描述返回结构都是描述data里面的内容**
 
|名称|类型|说明|
|-|-|-|
|code| int | 状态码，0为成功|
|data| json| 返回内容 |
|msg | string | 返回说明 |
|timestamp | int | 返回时间戳 |

成功样例

```
{
    "code": 200,
    "msg": "请求成功",
    "data": [],
    "timestamp": 1631254154
}
```

失败样例（带数据据返回）
```
{
    "code": 422,
    "msg": "数据验证不正确",
    "data": {
        "name": [
            "帐号长度为11位"
        ]
    }
}
```

失败样例（不带数据据返回）

```
{
    "code": 404,
    "msg": "资源不存在",
    "data": ""
}
```

### 状态说明：

|code|说明|
|-|-|
| 0 | 成功|
|500| 服务务器错误 |
|501| 服务器繁忙 |
|401| 没有授权 |
|402| 权限不足，没有权限操作此资源 |
|404| 资源不存在 |
|405| 方法不允许 |
|422| 字段验证失败 |
|419| 请求次数过多 |

### 授权说明：
请注意写有面要授权的接品，需要带上token格式如果放在header格式如上

|名称|类型|传输方式|说明|
|-|-|-|-|
|Authorization| sring | head,post|Bearer $token|

```

{
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODEvYXBpL2F1dGgvbG9naW4vdG9rZW4iLCJpYXQiOjE2MzI4MjI3OTMsImV4cCI6MTYzMjgyNjM5MywibmJmIjoxNjMyODIyNzkzLCJqdGkiOiJ5c3VGaVFGZjJOQXJGQjM5Iiwic3ViIjoxLCJwcnYiOiJlMzkxMzU1ZTRmZGUxY2YyMWVkNzFiODM1ZTJmMzA1Y2M2N2Q3Y2M2IiwiMCI6IiJ9.1SJcsKW-JAoa1wtYAR5ol0DYkYbABsKiqSs3MpsGkvA
}
```

如果放在请求头上则直接,?token=$token,如

```
http://www.robot.com/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODEvYXBpL2F1dGgvbG9naW4vdG9rZW4iLCJpYXQiOjE2MzI4MjI3OTMsImV4cCI6MTYzMjgyNjM5MywibmJmIjoxNjMyODIyNzkzLCJqdGkiOiJ5c3VGaVFGZjJOQXJGQjM5Iiwic3ViIjoxLCJwcnYiOiJlMzkxMzU1ZTRmZGUxY2YyMWVkNzFiODM1ZTJmMzA1Y2M2N2Q3Y2M2IiwiMCI6IiJ9.1SJcsKW-JAoa1wtYAR5ol0DYkYbABsKiqSs3MpsGkvA

```

## 授权模块
授权流程
用用户名帐号密码登录拿到token值  

### 用户登录
接口地址：/auth/token/login  
返回格式:josn  
是否需要授权：否  
请求方式：post  
请求头：  

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|username| string| josn | 是 | 登录帐号 |
|password| string| josn | 是 | 登录密码 |

返回格式:

| 字段 | 类型 | 说明 |
|-|-|-|
| token_type | string | token类型|
| expires_in | int | token到期时间|
| access_token | string | token值|

请求样例：

```
{
    "username": "admin",
    "password": "admin888"
}
```

返回样例：

```
{
    "code": 0,
    "data": {
        "token_type": "Bearer",
        "expires_in": 31536000,
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5NDNjNzk2Mi1kYmYyLTQ3ZTQtYTA3OC1mNDcwMTY3NGRiNDYiLCJqdGkiOiIwZDU4ODE5MjA0ZjEzOWViOWQxNmJhNTUyZThiNGIyNzU1MjllOGNhZGE1NDc2YjI0YTI0ZmQxM2FjMDEwZDYxNzdiNDRjNTMyOGM4YzU2NSIsImlhdCI6MTYzMTI0NTgzMywibmJmIjoxNjMxMjQ1ODMzLCJleHAiOjE2NjI3ODE4MzMsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.LYE80c7IQe9qqLc5Kb0qPdoCouHDC13zWgqBNRjsD133UyB_hSJLPftzb2RP4HEzwcvdFs1WmurWIk5iWSxFkwDqdZSNeIPxXp6qMubN1zVwD-X-B2-dh0Hu7tS9LcAraOhZDz0_Wqf8YoOn4NyMp_ds2gvaADP9Xkpxexeg3eoV78lJb4Sx89L00aEiAxn2HattueA_fatVKxztueU_iH8u-25sB-vJFKdmoXIV-VF9EH4inxDh6hPvFBPT_iH6YFzBFHBSln7bu7JBudXamFT4fg0d5dubDVs78ZX4uSD0CPEyda7CY39UDFkICT8ltKCcF_zHsCc0FcSjU2G2KomB9eHo7Ga3L99M4I0KqrXnFKAHq44G0vOP1k4PYlxsISqOPeqILYck30iszttRbxpBSu4YIf0-i2nO07vsjrVW-LXKXQPqeUQTebgnTgPi0nZz7Svh7BWrBhFP3iaERQKUbAnLOSuy-zLPf3j10L-Ap9PGBJMny6w6T-7Okld7WTYjpv5R3TT6i7PnyatBQosLbxkSXuiOpilsMNOaWa0Yv73c31TLHwX1u70rPXPq5p9VRHnZJPzxveWdBP5atBNELpiTXiZwup8eThj64O80QbN86SAuCJF9Sl6Yhim6BMQIwVDUnaWxKFhgTuChgK5KqkoYPymnQZj5heGl1mM"
    },
    "msg": "success",
    "timestamp": 1631245833
}
```

### 用户退出
接口地址：/auth/logout  
返回格式:josn  
是否需要授权：否  
请求方式：any  
请求样例：  

```
/auth/logout
```

返回样例：

```
{
    "code": 0,
    "msg": "退出成功",
    "data": [],
    "timestamp": 1641190457
}
```

## 个人中心模块

### 个人信息
接口地址：/me/profile/user  
返回格式:josn  
是否需要授权：是  
请求方式：get  

返回格式:

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 用户id|
| name | string | 帐号|
| nickname | string | 昵称|
| phone | string | 手机|
| email | sring | 邮箱 |
| role | object | 所属角色|
| avatar | string | 头像|
| last_at | datetime | 最近登录时间|
| last_ip | string | 最近登录ip|
| login_times | int | 登录次数|
| passed | int | 是否审核|
| is_system | boolean | 是否为系统用户|
| desc | string | 用户描述|
| created_at | datetime | 创建时间|
| updated_at | datetime | 修改时间|

请求样例：

```
/me/profile/user
```

返回样例：

```
{
    "code": 0,
    "msg": "获取个人信息成功",
    "data": {
        "id": 8,
        "name": "test",
        "nickname": "测试用户",
        "phone": "13829298899",
        "email": "test@163.com",
        "avatar": "/abc/img.jpg",
        "role": {
            "id": 4,
            "name": "测试角色",
            "desc": "测试一下",
            "is_system": 0,
            "created_at": "2021-12-31T11:52:47.000000Z",
            "updated_at": "2022-01-03T07:55:58.000000Z"
        },
        "last_at": null,
        "last_ip": "",
        "login_times": 0,
        "passed": 1,
        "is_system": 0,
        "desc": "测试",
        "created_at": "2022-01-03T08:42:39.000000Z",
        "updated_at": "2022-01-03T08:42:39.000000Z"
    },
    "timestamp": 1641199689
}
```

### 修改个人信息
接口地址：/me/profile/user  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|nickname| string| josn | 否 | 昵称 |
|avatar| string| josn | 否 | 头像 |
|phone| string| josn | 否 | 手机 |
|email| string| josn | 否 | 电子邮件 |
|desc| string| josn | 否 | 用户描述 |

请求样例：

```
{
    "nickname":"测试用户",
    "avatar":"测试用户",
    "email":"test@163.com",
    "phone":"13800000000",
    "desc":"用此用户测试功能"
}
```

返回样例：
```
{
    "code": 0,
    "msg": "修改个人信息成功",
    "data": [],
    "timestamp": 1640947067
}
```

### 修改密码
接口地址：/me/profile/password  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|password| string| josn | 是 | 新密码 |

请求样例：

```
{
	"password":"3929699"
}
```

返回样例：

```
{
    "code": 0,
    "msg": "密码修改成功",
    "data": [],
    "timestamp": 1640947067
}
```

### 上传头像
接口只是上传并没有保存到数据库，要保存数据需要调用个人信息接口把返回的图像路径附加进去  
接口地址：/me/profile/avatar  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|file| file| josn | 是 | 新密码 |

请求样例：
```
{
	"file":object
}
```
返回样例：
```
{
    "code": 0,
    "msg": "头像上传完成",
    "data": "/upload/avatar/roWABtoO4M2o2kiUqGL5n2VExhafI6nskeVp3voH.png",
    "timestamp": 1641201080
}
```

### 个人菜单

接口地址：/me/profile/menu  
返回格式:josn  
是否需要授权：是  
请求方式：get  

请求样例：

```
/me/menu
```

返回样例：

```
{
    "code": 0,
    "msg": "菜单获取成功",
    "data": [
        {
            "id": 1,
            "parent_id": 0,
            "name": "权限管理",
            "desc": null,
            "prefix": "/",
            "path": "",
            "icon": "",
            "target": "_self",
            "order": 1,
            "status": 1,
            "is_system": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 2,
            "parent_id": 1,
            "name": "系统用户",
            "desc": null,
            "prefix": "/",
            "path": "member/user",
            "icon": "",
            "target": "_self",
            "order": 1,
            "status": 1,
            "is_system": 1,
            "created_at": null,
            "updated_at": null
        }
    ],
    "timestamp": 1641054427
}
```

### 通知公告

接口地址：/me/notice  
返回格式:josn  
是否需要授权：是  
请求方式：get  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|page| int| params | 否 | 默认当前页 |
|limit| int| params | 否 | 几条一页，默认10条一页 |
|key| string| params | 否 | 查询关键字，搜索标题值 |
|type| string| params | 否 | 通知类型，默认全部类型 |
|read| enum| params | 否 | 是否已读，默认全部 |

返回说明

| 参数 | 类型|  说明 |
|-|-|-|
|data| array | 返回数据据 |
|total| int | 数据总数 |
|page| int| 当前页 |
|limit| int | 一页条数 |

data说明 
参考通知详情

请求样例：
```
/me/notice
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": {
        "data": [
            {
                "id": 1,
                "user_id": 1,
                "title": "212121",
                "content": "211212",
                "is_read": 0,
                "type": 1,
                "deleted_at": null,
                "created_at": null,
                "updated_at": null
            }
        ],
        "total": 1,
        "page": 1,
        "limit": "10"
    },
    "timestamp": 1641112749
}
```

### 通知详情

接口地址：/me/notice/{id}  
返回格式:josn  
是否需要授权：是  
请求方式：get  
返回格式:

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 用户id|
| user_id | int | 所属用户|
| title | string | 通知标题|
| content | string | 通知内容|
| is_read | boolean | 是否已读 |
| type | int | 通知类型|
| deleted_at | datetime | 头像|
| created_at | datetime | 创建时间|
| updated_at | datetime | 修改时间|

请求样例：
```
/me/notice/1
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": {
        "id": 1,
        "user_id": 1,
        "title": "212121",
        "content": "211212",
        "is_read": 0,
        "type": 1,
        "deleted_at": null,
        "created_at": null,
        "updated_at": null
    },
    "timestamp": 1641107877
}
```
### 设置已读
接口地址：/me/notice  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求样例：
```
[1,2,3]
```
返回样例：
```
{
    "code": 0,
    "msg": "设置已读成功",
    "data": [],
    "timestamp": 1640947067
}
```
### 恢复删除
接口地址：/me/notice  
返回格式:josn  
是否需要授权：是  
请求方式：patch  
请求样例：
```
[1,2,3]
```
返回样例：
```
{
    "code": 0,
    "msg": "设置已读成功",
    "data": [],
    "timestamp": 1640947067
}
```

### 删除通知
接口地址：/me/notice  
返回格式:josn  
是否需要授权：是  
请求方式：delete  
请求样例：
```
[1,2,3]
```
返回样例：
```
{
    "code": 0,
    "msg": "删除成功",
    "data": [],
    "timestamp": 1640947067
}
```


## 权限管理

### 所有成员
接口地址：/member/user  
返回格式:josn  
是否需要授权：是  
请求方式：get  
请求头：  

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|page| int| params | 否 | 默认当前页 |
|limit| int| params | 否 | 几条一页，默认10条一页 |
|key| string| params | 否 | 查询关键字，搜索name,phone值 |
|passed| string| params | 否 | 是否审核，-1全部,0未审核,1已审核。默认-1全部用户 |
|sort| enum| params | 否 | 要排序字段，只能是id,last_at,login_times,created_at默认值为id |
|order| enum| params | 否 | 排序方法 desc asc,默认为desc |

返回说明

| 参数 | 类型|  说明 |
|-|-|-|
|data| array | 返回数据 |
|total| int | 数据总数 |
|page| int| 当前页 |
|limit| int | 一页条数 |

返回data说明

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 用户id |
| name | string | 用户帐号|
| nickname | string | 昵称|
| phone | string | 用户手机号|
| email | string | 用户邮箱|
| avatar | string | 头像|
| role | object | 所在角色|
| last_at | datetime | 最近登录时间|
| login_times | int | 部登录次数|
| passed | boolean | 是否审核|
| desc | string | 用户描述|
| is_system | boolean | 是否是系统用户|
| created_at | datetime | 创建时间|
| updated_at | datetime | 最近更新时间|

role说明
请参考角色字段说明

请求样例：
```
/member/user?page=1&limit=10&key=te&passed=1&sort=created_at&order=desc
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": {
        "data": [
            {
                "id": 6,
                "name": "test",
                "nickname": "测试用户",
                "phone": "13800000000",
                "email": "test@163.com",
                "avatar": "/abc/img.jpg",
                "role": {
                    "id": 4,
                    "name": "rrtrtrtr",
                    "desc": "测试一下",
                    "status": 1,
                    "is_system": 0,
                    "created_at": "2021-12-31T11:52:47.000000Z",
                    "updated_at": "2022-01-03T06:32:01.000000Z"
                },
                "last_at": null,
                "last_ip": "",
                "login_times": 0,
                "passed": 1,
                "is_system": 0,
                "desc": "测试",
                "created_at": "2022-01-03T07:42:44.000000Z",
                "updated_at": "2022-01-03T07:42:44.000000Z"
            }
        ],
        "total": 1,
        "page": 1,
        "limit": "10"
    },
    "timestamp": 1641195784
}
```

### 添加成员
接口地址：/member/user  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|name| string| josn | 是 | 帐号名 |
|role_id| int| josn | 是 | 角色id |
|password| string| josn | 是 | 用户密码 |
|nickname| string| josn | 否 | 昵称 |
|avatar| string| josn | 否 | 用户头像 |
|phone| string| josn | 否 | 用户手机 |
|email| string| josn | 否 | 用户邮件 |
|desc| string| josn | 否 |用户描述 |
|passed| boolean| josn | 否 | 是否审核，默认1 |

请求样例：
```
{
	"name":"test",
	"role_id":1,
	"password":"test1000",
	"nickname":"好人",
	"avatar":"/abc/img.jpg",
	"phone":"13800000000",
	"email":"test@163.com",
	"desc":"测试",
	"passed":1
}
```
返回样例：
```
{
    "code": 0,
    "msg": "保存用户成功",
    "data": [],
    "timestamp": 1640937889
}
```

### 修改成员
接口地址：/member/user  
返回格式:josn  
是否需要授权：是  
请求方式：put  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|id| int| josn | 是 | 用户id |
|role_id| int| josn | 是 | 角色id |
|password| string| josn | 否 | 用户密码，为空时不修改密码 |
|nickname| string| josn | 否 | 昵称 |
|avatar| string| josn | 否 | 用户头像 |
|phone| string| josn | 否 | 用户手机 |
|email| string| josn | 否 | 用户邮件 |
|desc| string| josn | 否 |用户描述 |
|passed| boolean| josn | 否 | 是否审核，默认1 |

请求样例：
```
{
	"id":2,
	"name":"test",
	"role_id":1,
	"password":"test1000",
	"nickname":"好人",
	"avatar":"/abc/img.jpg",
	"phone":"13800000000",
	"email":"test@163.com",
	"desc":"测试",
	"passed":1
}
```
返回样例：
```
{
    "code": 0,
    "msg": "保存用户成功",
    "data": [],
    "timestamp": 1640937889
}
```

### 删除成员
接口地址：/member/user  
返回格式:josn  
是否需要授权：是  
请求方式：delete  


请求样例：
```
[1,7]
```
返回样例：
```
{
    "code": 0,
    "msg": "删除用户成功",
    "data": [],
    "timestamp": 1640950217
}
```
### 所有角色
接口地址：/member/role  
返回格式:josn  
是否需要授权：是  
请求方式：get  
请求头：  

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|page| int| params | 否 | 默认当前页 |
|limit| int| params | 否 | 几条一页，默认10条一页 |

返回说明

| 参数 | 类型|  说明 |
|-|-|-|
|data| array | 返回数据据 |
|total| int | 数据总数 |
|page| int| 当前页 |
|limit| int | 一页条数 |

返回data说明

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 角色id |
| name | string | 角色名|
| desc | string | 角色描述|
| is_system | boolean | 是否是系统角色|
| access | array | 权限|
| created_at | datetime | 创建时间|
| updated_at | datetime | 最近更新时间|

access说明
请参考权限说明

请求样例：
```
/member/user?page=1&limit=10&key=te&passed=1&sort=created_at&order=desc
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": {
        "data": [
            {
                "id": 3,
                "name": "9999",
                "desc": "",
                "is_system": 0,
                "access": [
                    {
                        "id": 1,
                        "parent_id": 0,
                        "name": "用户管理",
                        "code": "safe",
                        "path": "/member/user",
                        "method": "GET",
                        "desc": "",
                        "created_at": null,
                        "updated_at": null,
                        "pivot": {
                            "role_id": 3,
                            "access_id": 1
                        }
                    }
                ],
                "created_at": "2021-12-30T21:00:12.000000Z",
                "updated_at": "2021-12-30T21:00:12.000000Z"
            }
        ],
        "total": 1,
        "page": 1,
        "limit": 10
    },
    "timestamp": 1640950853
}
```

### 添加角色
接口地址：/member/role  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|name| string| josn | 是 | 角色名 |
|access| array| josn | 否 | 角色权限 |
|desc| string| josn | 否 |角色描述 |

请求样例：
```
{
	"name":"测试角色",
	"desc":"测试角色描述",
	"access":[1]
}
```
返回样例：
```
{
    "code": 0,
    "msg": "保存角色成功",
    "data": [],
    "timestamp": 1640951567
}
```

### 修改角色
接口地址：/member/role  
返回格式:josn  
是否需要授权：是  
请求方式：put  
请求头：

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|id| int| josn | 是 | 角色id |
|name| string| josn | 是 | 角色名 |
|access| array| josn | 否 | 角色权限 |
|desc| string| josn | 否 |角色描述 |

请求样例：
```
{
	"id":2,
	"name":"测试角色2",
	"desc":"测试角色描述2",
	"access":[1]
}
```
返回样例：
```
{
    "code": 0,
    "msg": "保存角色成功",
    "data": [],
    "timestamp": 1640951567
}
```

### 删除角色
接口地址：/member/role  
返回格式:josn  
是否需要授权：是  
请求方式：delete  

请求样例：

```
[1,7]
```

返回样例：

```
{
    "code": 0,
    "msg": "删除角色成功",
    "data": [],
    "timestamp": 1640951788
}
```

## 系统信息
### 系统配置
接口地址：/setting/config  
返回格式:josn  
是否需要授权：是  
请求方式：get  
返回格式:

| 字段 | 类型 | 说明 |
|-|-|-|
| group | array | 所属用户|
| data | array | 通知标题|

group说明

| 字段 | 类型 | 说明 |
|-|-|-|
| name | string | 分组名|
| label | string | 分组标题|

data说明

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 配置id|
| group_label | string | 分组名|
| group_name | string | 分组标题|
| name | string | 表单配置名|
| key | string | 配置识别值|
| value | string | 配置设置值|
| field_type | enum | 内容类型,text,select,radio|
| sort | string | 排序|
| default_value | string | 默认值|
| option_value | string | 项值|
| is_public | string | 公开|
| help | string | 提示|
| created_at | datetime | 创建时间|
| updated_at | datetime | 修改时间|

请求样例：
```
/setting/config
```
返回样例：
```
{
    "code": 0,
    "msg": "获取配置成功",
    "data": {
        "group": {
            "system": {
                "name": "system",
                "label": "系统"
            }
        },
        "data": {
            "system": [
                {
                    "id": 1,
                    "group_label": "系统",
                    "group_name": "system",
                    "name": "网站名",
                    "key": "app.name",
                    "value": "heekit商城22",
                    "field_type": "text",
                    "sort": 0,
                    "default_value": "heekit商城",
                    "option_value": null,
                    "is_public": 0,
                    "help": "请输入站点名22",
                    "created_at": null,
                    "updated_at": "2021-12-31T09:26:56.000000Z"
                },
                {
                    "id": 2,
                    "group_label": "系统",
                    "group_name": "system",
                    "name": "DEBUG",
                    "key": "app.debug",
                    "value": "0",
                    "field_type": "switch",
                    "sort": 1,
                    "default_value": "0",
                    "option_value": null,
                    "is_public": 0,
                    "help": null,
                    "created_at": null,
                    "updated_at": "2021-12-31T09:26:56.000000Z"
                }
            ]
        }
    },
    "timestamp": 1641139850
}
```

### 保存配置
接口地址：/setting/config  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：array  

数组项说明：

| 字段 | 类型 | 方式|是否必填|说明 |
|-|-|-|-|-|
| id | int | josn|否|配置id|
| value | string |  josn|否|配置设置值|

请求样例：
```
 [{
	"id": 1,
	"value": "heekit商城22",
}, {
	"id": 2,
	"value": "0",
}, {
	"id": 3,
	"value": ""
}, {
	"id": 4,
	"value": ""
}, {
	"id": 5,
	"value": ""
}]
```
返回样例：
```
{
    "code": 0,
    "msg": "保存配置完成",
    "data": [],
    "timestamp": 1641145092
}
```
### 上传配置图片
接口地址：/setting/config/upload  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头： 

| 参数 | 类型| 方式 | 是否必填 | 说明 |
|-|-|-|-|-|
|file| file| josn | 是 | 文件 |  


请求样例：
```
{
	"file":object
}
```
返回样例：
```
{
    "code": 0,
    "msg": "上传配置图片成功",
    "data": "/upload/eZmlSRHCWgk43g9n2wNnD9OAj7pf3ZN0n4ashU0J.jpg",
    "timestamp": 1641381602
}
```


### 获取菜单
接口地址：/setting/menus  
返回格式:josn  
是否需要授权：是  
请求方式：get  
返回格式:

| 字段 | 类型 | 说明 |
|-|-|-|
| id | int | 菜id|
| name | string | 所属用户|
| parent_id | int | 父级菜单|
| desc | string | 菜单描述|
| prefix | string | 前缀|
| path | string | 菜单路径|
| icon | string | 菜单标|
| target | string | 打开目标，_self,_blank|
| order | int | 排序|
| status | boolean | 菜单状态，是否显示|
| access | array | 菜单关联权限|
| is_system | string | 是否系统菜单 |
| created_at | datetime | 创建时间|
| updated_at | datetime | 修改时间|

access说明
请参考权限表

请求样例：
```
/setting/menus
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": [
        {
            "id": 1,
            "name": "权限管理",
            "parent_id": 0,
            "desc": null,
            "prefix": "/",
            "path": "",
            "icon": "",
            "target": "_self",
            "order": 1,
            "status": 1,
            "access": [],
            "is_system": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 2,
            "name": "系统用户",
            "parent_id": 1,
            "desc": null,
            "prefix": "/",
            "path": "member/user",
            "icon": "",
            "target": "_self",
            "order": 1,
            "status": 1,
            "access": [
                {
                    "id": 6,
                    "parent_id": 5,
                    "name": "添加角色",
                    "code": "",
                    "path": "/member/role",
                    "method": "POST",
                    "desc": "",
                    "created_at": null,
                    "updated_at": null,
                    "pivot": {
                        "menu_id": 2,
                        "access_id": 6
                    }
                }
            ],
            "is_system": 1,
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 3,
            "name": "角色信息",
            "parent_id": 1,
            "desc": null,
            "prefix": "/",
            "path": "member/role",
            "icon": "",
            "target": "_self",
            "order": 1,
            "status": 1,
            "access": [],
            "is_system": 1,
            "created_at": null,
            "updated_at": null
        }
    ],
    "timestamp": 1641157397
}
```

### 添加菜单
接口地址：/setting/menu  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 字段 | 类型 | 方式|是否必填|说明 |
|-|-|-|-|-|
| name | string | josn|是|所属用户|
| parent_id | int | josn|否|父级菜单|
| desc | string | josn|否|菜单描述|
| prefix | string | josn|否|前缀|
| path | string | josn|否|菜单路径|
| icon | string | josn|否|菜单标|
| target | string | josn|否|打开目标，_self,_blank|
| order | int | josn|否|排序|
| status | boolean | josn|否|菜单状态，是否显示|
| access | array | josn|否|菜单关联权限|

请求样例：
```
{
	"name":"测试菜单",
	"parent_id":1,
	"prefix":"/",
	"path":"/system/log",
	"icon":"md_person",
	"target":"_self",
	"order":0,
	"status":1,
	"desc":"测试菜单说明",
	"access":[1]
}

```
返回样例：
```
{
    "code": 0,
    "msg": "菜单保存成功",
    "data": [],
    "timestamp": 1641159251
}
```

### 修改菜单
接口地址：/setting/menu  
返回格式:josn  
是否需要授权：是  
请求方式：put  
请求头：

| 字段 | 类型 | 方式|是否必填|说明 |
|-|-|-|-|-|
| id | int |josn|是| 菜id|
| name | string | josn|是|所属用户|
| parent_id | int | josn|否|父级菜单|
| desc | string | josn|否|菜单描述|
| prefix | string | josn|否|前缀|
| path | string | josn|否|菜单路径|
| icon | string | josn|否|菜单标|
| target | string | josn|否|打开目标，_self,_blank|
| order | int | josn|否|排序|
| status | boolean | josn|否|菜单状态，是否显示|
| access | array | josn|否|菜单关联权限|

请求样例：
```
{
	"id":10,
	"name":"测试菜单",
	"parent_id":1,
	"prefix":"/",
	"path":"/system/log",
	"icon":"md_person",
	"target":"_self",
	"order":0,
	"status":1,
	"desc":"测试菜单说明",
	"access":[1]
}
```
返回样例：
```
{
    "code": 0,
    "msg": "菜单保存成功",
    "data": [],
    "timestamp": 1641159251
}
```

### 删除菜单
接口地址：/setting/menus  
返回格式:josn  
是否需要授权：是  
请求方式：delete  

请求样例：
```
[11]
```
返回样例：
```
{
    "code": 0,
    "msg": "删除菜单成功",
    "data": [],
    "timestamp": 1641159539
}
```
### 获取权限
接口地址：/setting/access  
返回格式:josn  
是否需要授权：是  
请求方式：get  
返回格式:array  

| 字段 | 类型 |说明 |
|-|-|-|
| id | int| 菜id|
| name | string |权限名|
| parent_id | int|父级权限|
| desc | string|权限描述|
| code | string|权限识别码|
| path | string |权限关联路径|
| method | string |请求方法|
| created_at | datetime | 创建时间|
| updated_at | datetime | 修改时间|

请求样例：
```
/setting/access
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": [
        {
            "id": 1,
            "parent_id": 0,
            "name": "用户管理",
            "code": "safe",
            "path": "/member/user",
            "method": "GET",
            "desc": "",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 2,
            "parent_id": 1,
            "name": "添加用户",
            "code": "user",
            "path": "/member/user",
            "method": "GET",
            "desc": "",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 3,
            "parent_id": 1,
            "name": "修改用户",
            "code": "role",
            "path": "/member/user",
            "method": "PUT",
            "desc": "",
            "created_at": null,
            "updated_at": null
        },
        {
            "id": 4,
            "parent_id": 1,
            "name": "删除用户",
            "code": "role",
            "path": "/member/user",
            "method": "DELETE",
            "desc": "",
            "created_at": null,
            "updated_at": null
        }
    ],
    "timestamp": 1641161061
}
```

### 添加权限
接口地址：/setting/access  
返回格式:josn  
是否需要授权：是  
请求方式：post  
请求头：

| 字段 | 类型 | 方式|是否必填|说明 |
|-|-|-|-|-|
| name | string | josn|是|权限名|
| parent_id | int | josn|否|父级权限|
| desc | string | josn|否|权限描述|
| code | string | josn|否|权限识别码|
| path | string | josn|是|权限关联路径|
| method | string | josn|是|请求方法，post,head,put,get,delete|

请求样例：
```
{
    "name":"测试权限",
    "parent_id":5,
    "code":"258",
    "path":"/abde",
    "method":"POST"
}
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": "权限保存成功",
    "timestamp": 1641161537
}
```

### 修改权限
接口地址：/setting/access  
返回格式:josn  
是否需要授权：是  
请求方式：put  
请求头：

| 字段 | 类型 | 方式|是否必填|说明 |
|-|-|-|-|-|
| id | int |josn|是| 菜id|
| name | string | josn|是|权限名|
| parent_id | int | josn|否|父级权限|
| desc | string | josn|否|权限描述|
| code | string | josn|否|权限识别码|
| path | string | josn|是|权限关联路径|
| method | string | josn|是|请求方法，post,head,put,get,delete|

请求样例：

```
{
    "name":"测试权限",
    "parent_id":5,
    "code":"258",
    "path":"/abde",
    "method":"POST"
}
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": "权限保存成功",
    "timestamp": 1641161537
}
```

### 删除权限

接口地址：/system/access  
返回格式:josn  
是否需要授权：是  
请求方式：delete  

请求样例：
```
[11]
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": "删除权限成功",
    "timestamp": 1641161133
}
```
### 获取路由

接口地址：/setting/uris  
返回格式:josn   
是否需要授权：是  
请求方式：get  
返回格式:  

| 字段 | 类型 |说明 |
|-|-|-|
| method | string |请求方法|
| path | string|请求路径|


请求样例：
```
/setting/uris
```
返回样例：
```
{
    "code": 0,
    "msg": "",
    "data": [
        {
            "method": "GET|HEAD",
            "path": "api/member/user"
        },
        {
            "method": "POST",
            "path": "api/member/user"
        }
    ],
    "timestamp": 1641381666
}
```