<?php
namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable  implements JWTSubject
{
	use Notifiable,HasFactory;
	  
	protected $fillable = ['name','password','nickname','role_id','avatar','phone','email','desc','passed'];
	
    protected $hidden = ['password', 'remember_token'];
	
	public function role()
    {
         return $this->belongsTo('App\Model\Role');
    }
	
	public function menus()
    {
		if($this->id==1){
			return Menu::where('status',1)->get();		
		}elseif($this->role){
			return $this->role->menus();	
		}else{
			return ;
		}      
    }
	
	public function check($path,$method)
    {
	  if($this->id==1){
		return true; 
	  }
	  if($this->role->access->where('path',$path)->where('method',implode('|',$method))->first()){
		return true;   
	  }
      return false; 
    }
	
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
	
    public function getJWTCustomClaims()
    {
        return [''];
    }
}
