<?php
namespace App\Repositories;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\Model\User;

class Auth implements Repository
{
	/*保存用户*/
	public function store($user,$notify){
		
		$user->login_times=$user->login_times+1;
		
		if($user->is_first===0){
			$user->is_first=1;
		}elseif($user->is_first===1){
			$user->is_first=2;
		}
		
		$user->save();
	}
	
	/*删除用户*/
	public function remove($id,$notify){}
	
	public function login($data,$request){
		
		if(!$user = User::where("name",$data["name"])->first()){
            throw ValidationException::withMessages(["name" => "帐号密码不正确"]);
		}
		if(!Hash::check($data["password"], $user->password)){
            throw ValidationException::withMessages(["name" => "帐号密码不正确"]);
		}
		
		if($user->passed!=1){throw ValidationException::withMessages(["name" => "该号已被禁用,联系管理员开通"]);}

		$token = auth("api")->login($user);

		$notify["login"]="password";
		
		$user->last_ip=$request->ip();

		$user->last_at=date('Y-m-d H:i:s', time());
		
		$this->store($user,$notify);
		
		return $token;
		
	}
}
