<?php
namespace App\Repositories;
use Illuminate\Support\Facades\Hash;
use App\Events\UserStored;
use App\Events\UploadStored;
use App\Events\NoticeChanged;
use App\Model\User;
use App\Model\Notice;
use Storage;
use Illuminate\Validation\ValidationException;


class Profile implements Repository
{
	private $user;
	
    public function __construct(User $user)
    {
        $this->user=$user;
    }
	
	/*保存用户*/
	public function store($data,$notify){
		
	  isset($data["nickname"])?$this->user->nickname=$data["nickname"]:'';
		
	  isset($data["avatar"])?$this->user->avatar=$data["avatar"]:'';
	  
	  isset($data["phone"])?$this->user->phone=$data["phone"]:'';
	  
	  isset($data["email"])?$this->user->email=$data["email"]:'';
	  
	  isset($data["desc"])?$this->user->desc=$data["desc"]:'';
		
      $this->user->save();

	  $notify["method"]="profile";

	  event(new UserStored($this->user,$notify));

	  return true ;
	}
	
	/*修改密码*/
	public function password($data,$notify){

	  $data["password"]?$this->user->password=Hash::make($data["password"]):'';
	  
      $this->user->save();

	  $notify["method"]="password";

	  event(new UserStored($this->user,$notify));

	  return true ;
	}
	
	/*上传头象*/
	public function avatar($request,$notify){
	  
	  $path=$request->file->store('avatar',config("shop")["avatar"]);
	  
	  $url= config("filesystems")["disks"][config("shop")["avatar"]]["url"]."/". $path;
		  
	  event(new UploadStored(["path"=>$path,"url"=>$url],$notify));

	  return $url;
	}
	
	/*删除已读通知*/
	public function remove($id,$notify)
	{
		if(!$notice=Notice::whereIn("id",$id)->where("user_id",$this->user->id)->get()){
			return true;
		}
		
		Notice::whereIn("id",$id)->where("user_id",$this->user->id)->delete();
		
		$notify["method"]="remove";
		
		event(new NoticeChanged($notice,$notify));
		
		return true;
	}
	
	/*标志已读通知*/
	public function read($id,$notify)
	{
		if(!$notice=Notice::whereIn("id",$id)->where("user_id",$this->user->id)->get()){
			return true;
		}
		
		Notice::whereIn("id",$id)->where("user_id",$this->user->id)->update(["is_read"=>1]);
		
		$notify["method"]="read";
		
		event(new NoticeChanged($notice,$notify));
		
		return true;
	}
	
	/*恢复删除*/
	public function restore($id,$notify)
	{
		if(!$notice=Notice::whereIn("id",$id)->where("user_id",$this->user->id)->get()){return true;}
		
		Notice::whereIn("id",$id)->where("user_id",$this->user->id)->restore();
		
		$notify["method"]="restore";
		
		event(new NoticeChanged($notice,$notify));
		
		return true;
	}
	/*设置当前用户*/
	public function setUser($user)
	{
		$this->user=$user;
	}
}
