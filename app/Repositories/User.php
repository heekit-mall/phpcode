<?php
namespace App\Repositories;
use Illuminate\Support\Facades\Hash;
use App\Model\User as userModel;
use App\Model\Role as roleModel;
use App\Events\UserStored;
use App\Events\UserRemoved;
use Illuminate\Validation\ValidationException;

class User implements Repository
{
	/*保存用户*/
	public function store($data,$notify){
		
		isset($data['password'])?$data['password']=Hash::make($data['password']):'';

		if(isset($data['id'])){
			
			$user=userModel::where("id",$data['id'])->first();
			
            if(isset($data["name"])){unset($data["name"]);}//修改用户时如果传了name则释放掉name
			
			$user->update($data);
			
			$notify["method"]="edit";
		
		}else{
			
			$user=userModel::create($data);
				
			$notify["method"]="add";
		}
		
		event(new UserStored($user,$notify));
	
		return true ;
	}
	
	/*删除用户*/
	public function remove($id,$notify)
	{
		if(!$user=userModel::whereIn("id",$id)->where("is_system","<>",1)->get()){
			return true;
		}
		
		userModel::whereIn("id",$id)->where("is_system","<>",1)->delete();
		
		event(new UserRemoved($user,$notify));
		  
		return true;
	}
}
