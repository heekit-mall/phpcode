<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class ProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        return  [
				'nickname'              => 'max:100', 
				'avatar' 				=> 'max:254',	
				'email' 				=> 'max:100|email',
                'phone'                 => 'size:11|unique:users,phone,'.$this->user()->id,
                'desc'					=> 'max:255'
            ];
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nickname.max'        => '昵称过长，长度不得超出10',
			
			'avatar.max' 		 => '图片路径不能超过254个字符',
			
            'email.required' => '邮箱必填',
			'email.max' => '邮箱长度不能超过100',
            'email.email'   => '请输入正确的邮箱',
			
            'phone.required'     => '请填写手机号码',
            'phone.size'         => '国内的手机号码长度为11位',
            'phone.mobile_phone' => '请填写合法的手机号码',
            'phone.unique'       => '此手机号码已存在于系统中，不能再进行二次关联',
			
            'desc.max'   		 => '用户描述长度不得超出255'
        ];
    }
}
