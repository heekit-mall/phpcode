<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class RoleRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        //store		
        if($this->method()==="POST"){
            $rules = [            
				'name'			=> 'required|min:4|max:20|unique:roles,name',
				'access'		=> 'array',
				'access.*'		=> 'integer|exists:accesses,id',
                'desc'			=> 'max:255'
            ];
        }else{
            $rules = [
                'id'			=> 'required|integer|exists:roles,id',
                'name'          => 'required|min:4|max:20|unique:roles,name,'.$this->id,
				'access.*'		=> 'integer|exists:accesses,id',
                'desc'			=> 'max:255'
            ];
        }
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'      	  => '角色id必填',
            'id.integer'      	  => '角色id必须为整数',
            'id.exists'      	  => '角色不存',
			
            'name.unique'        => '此角色名已存在，请尝试其它名字组合',
            'name.required'      => '请填写角色名',
            'name.max'           => '角色名过长，长度不得超出20',
            'name.min'           => '角色名过短，长度不得少于4',
			
            'access.array'   	 => '必须为数组',
            'access.*.integer'   => '权限id必须为整数 ',
            'access.*.exists'    => '权限必须存在 ',
			
            'desc.max'   		 => '角色描述长度不得超出255'
        ];
    }
}
