<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class PasswordRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        //store		

		$rules = [
			'password'              => 'required|min:6|max:16|regex:/^[a-zA-Z0-9~@#%_]{6,16}$/i',
		];
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required'  => '密码必填',
            'password.max'           => '密码过长，长度不得超出16',
            'password.min'           => '密码过短，长度不得少于6',
            'password.regex'     => '密码包含非法字符，只能为英文字母(a-zA-Z)、阿拉伯数字(0-9)与特殊符号(~@#%_)组合',
        ];
    }
}
