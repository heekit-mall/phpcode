<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class MenuRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        //store		
        if($this->method()==="POST"){
			
            $rules = [            
				'name'                  => 'required|min:2|max:20|unique:menus,name',
				'parent_id'             => 'integer|'.($this->parent_id>0?Rule::exists('menus','id')->where('parent_id',0):''),
				'desc'                  => 'max:254',
				'prefix'                => 'max:254',
				'path'                  => 'max:254',
				'icon'                  => 'max:254',
				'target'                => 'max:254',
				'order'                 => 'integer',
				'access'                => 'array',
				'access.*'		=> 'integer|exists:accesses,id',
				'status'                => 'boolean'
            ];
        }else{//store
            $rules = [
				'id'				    =>'required|integer|exists:menus,id',
                'name'                  => 'required|min:2|max:20|unique:menus,name,'.$this->id,
				'parent_id'             => 'integer|different:id|'.($this->parent_id>0?Rule::exists('menus','id')->where('parent_id',0):''),
				'desc'                  => 'max:254',
				'prefix'                => 'max:254',
				'path'                  => 'max:254',
				'icon'                  => 'max:254',
				'target'                => 'max:254',
				'order'                 => 'integer',
				'access'                => 'array',
				'access.*'				=> 'integer|exists:accesses,id',
				'status'                => 'boolean'
            ];
        }
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'     	 => 'id必填',
            'id.integer'     	 => 'id必须为整数',
            'name.required'      => '请填写菜单名名',
            'name.max'           => '菜单名过长，长度不得超出20',
            'name.min'           => '菜单名过短，长度不得少于2',
            'name.unique'        => '菜单名已存在',
			
            'parent_id.exists'   => '父级菜单不存在，或所选菜单是子级菜单',
			'parent_id.different'   => '父级菜单不能是自己',
			'parent_id.integer'   => '父级菜单id必须是整数',
			'desc.max'           => '权限描述过长，长度不得超出254',
			'prefix.max'         => '链接前缀，长度不得超出254',
			'path.max'           => '链接路径，长度不得超出254',
			'icon.max'           => '菜单图标，长度不得超出254',
			'target.max'         => '打开目标，长度不得超出254',
			'order.integer'      => '排序必须为整数',
			'access.array'       => '权限必须为数组',
            'access.*.integer'   => '权限id必须为整数 ',
            'access.*.exists'    => '权限必须存在 ',
			'status.boolean'     => '状态必须是布尔值',
			
        ];
    }
}
