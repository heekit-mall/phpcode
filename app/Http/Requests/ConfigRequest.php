<?php

namespace App\Http\Requests;

class ConfigRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        //store
            $rules = [
                '*.id'                	=> 'required|integer|exists:configs,id',
                '*.value'				=> 'max:255'
            ];
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.id.required'   => '配置id不能为空',
            '*.id.integer'   => '配置id必须为整数',
            '*.id.exists'   => '该配置不存',
            '*.value.max'   		 => '配置信息不能超过255个字符'
        ];
    }
}
