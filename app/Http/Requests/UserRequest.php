<?php

namespace App\Http\Requests;

class UserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        //store
        if($this->method()==="POST"){
			
            $rules = [            
				'name'                  => 'required|alpha_dash|min:4|max:20|unique:users,name',
                'password'              => 'required|min:6|max:16|regex:/^[a-zA-Z0-9~@#%_]{6,16}$/i',  //登录密码只能英文字母(a-zA-Z)、阿拉伯数字(0-9)、特殊符号(~@#%)
				'email' 				=> 'max:100|email',
				'avatar' 				=> 'max:254',	
                'role_id'               => 'required|integer|exists:roles,id',
                'passed'                => 'boolean',
				'nickname'              => 'max:100', 
                'phone'                 => 'size:11|unique:users,phone',
                'desc'					=> 'max:255'
            ];
        }else{//store
            $rules = [
                'id'                	=> 'required|integer|exists:users,id',
                'password'             	=> 'min:6|max:16|regex:/^[a-zA-Z0-9~@#%_]{6,16}$/i',  //登录密码只能英文字母(a-zA-Z)、阿拉伯数字(0-9)、特殊符号(~@#%)
				'email' 				=> 'max:100|email',
				'avatar' 				=> 'max:254',	
                'role_id'               => 'required|integer|exists:roles,id',
                'passed'                => 'boolean',
				'nickname'              => 'max:100', 
                'phone'                 => 'size:11|unique:users,phone,'.$this->id,
                'desc'					=> 'max:255'
            ];
        }
		
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'   => '用户id必传',
            'id.integer'   => '用户id必须正整数',
            'id.exists'   => '该用户不存在',
			
            'nickname.max'        => '昵称过长，长度不得超出100',

            'name.unique'        => '此登录名已存在，请尝试其它名字组合',
            'name.required'      => '请填写登录名',
            'name.max'           => '登录名过长，长度不得超出20',
            'name.min'           => '登录名过短，长度不得少于4',
            'name.alpha_dash' => '登录名只能阿拉伯数字与英文字母组合',

            'password.required'              => '请填写登录密码',
            'password.min'                   => '密码长度不得少于6',
            'password.max'                   => '密码长度不得超出16',
            'password.regex'                 => '密码包含非法字符，只能为英文字母(a-zA-Z)、阿拉伯数字(0-9)与特殊符号(~@#%_)组合',
			
			'email.max' => '邮箱长度不能超过100',
            'email.email'   => '请输入正确的邮箱',
			
			'avatar.max' 		 => '图片路径不能超过254个字符',
            'desc.max'   		 => '用户描述长度不得超出255',
			
            'role_id.required' => '请选择角色（用户组）',
            'role_id.integer' => '角色id必须为正整数',
            'role_id.exists'   => '系统不存在该角色',
			
            'phone.size'         => '国内的手机号码长度为11位',
            'phone.unique'       => '此手机号码已存在于系统中，不能再进行二次关联',

            'passed.boolean'  => '用户状态必须为布尔值',
        ];
    }
}
