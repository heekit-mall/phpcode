<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class AccessRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
        if($this->method()==="POST"){
			$rules = [            
				'name'                  => 'required|max:254',
				'parent_id'             => 'integer|'.($this->parent_id>0?Rule::exists('accesses','id')->where('parent_id',0):''),
				'code'             		=> 'max:254',
				'path' 					=> 'required|max:254',				
				'method'               	=> 'required|max:254',
				'desc' 					=> 'max:254',	
			];
		}else{
			$rules = [
				'id'				    =>'required|integer|exists:accesses,id',
				'name'                  => 'required|max:254',
				'parent_id'             => 'integer|different:id|'.($this->parent_id>0?Rule::exists('accesses','id')->where('parent_id',0):''),
				'code'             		=> 'max:254',
				'path' 					=> 'required|max:254',				
				'method'               	=> 'required|max:254',
				'desc' 					=> 'max:254',	
			];

		}
        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'     	 => '权限id必填',
            'id.integer'     	 => '权限id必须为整数',
            'id.exists'     	 => '权限不存在',
			
            'name.required'      => '功能名不能空',
			'name.max'           => '功能名长度不得超出254',
			
			'parent_id.integer'  => '父级权限id必须为整数',
			'parent_id.different'=> '父级权限不能为自己',
			'parent_id.exists'   => '父级权限不存在，或所选权限是子级权限',
			
			'code.max'           => '功能编号长度不得超出254',
			
            'path.required'      => '路径不能空',
			'path.max'           => '路径长度不得超出254',
			
            'path.required'      => '方法不能为空',
			'method.max'           => '方法长度不得超出254',
        ];
    }
}
