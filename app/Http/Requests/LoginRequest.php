<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

class LoginRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * 自定义验证规则rules
     *
     * @return array
     */
    public function rules()
    {
		$rules = [            
			'username'                  => 'required|alpha_dash|min:4|max:20',
			'password'             		=> 'required|min:6|max:16|regex:/^[a-zA-Z0-9~@#%_]{6,16}$/i'
		];

        return $rules;
    }

    /**
     * 自定义验证信息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required'      => '登录名不能为空',
			'username.alpha_dash'    => '登录名只能阿拉伯数字与英文字母组合',
            'username.min'      	 => '登录名长度不能小4',
			'username.max'   		 => '登录名长度不能大20',

			'password.required'  => '请填写登录密码',
			'password.min'		 => '密码长度不得少于6',
			'password.max'   	 => '密码长度不得超出16',
			'password.regex'  	 => '密码包含非法字符，只能为英文字母(a-zA-Z)、阿拉伯数字(0-9)与特殊符号(~@#%_)组合'
        ];
    }
}
