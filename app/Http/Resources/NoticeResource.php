<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class NoticeResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		 return [
           'id' => $this->id,
           'user_id' => $this->user_id,
           'title' => $this->title,
           'content' => $this->content,
           'is_read' => $this->is_read,	
           'type' => $this->type,	
		   'deleted_at' => $this->deleted_at,
           'created_at' => $this->created_at,
           'updated_at' => $this->updated_at
        ];
    }
}
