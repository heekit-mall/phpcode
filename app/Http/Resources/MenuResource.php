<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		 return [
           'id' => $this->id,
           'name' => $this->name,
           'parent_id' => $this->parent_id,
           'desc' => $this->desc,	
           'prefix' => $this->prefix,
           'path' => $this->path,	
           'icon' => $this->icon,
           'target' => $this->target,	
           'order' => $this->order,
           'status' => $this->status,	
           'access' => $this->access,
		   'is_system' => $this->is_system,
           'created_at' => $this->created_at,
           'updated_at' => $this->updated_at
        ];
    }
}
