<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Repositories\Auth;


class AuthController extends Controller
{
    public function __construct(Auth $auth,Request $request)
    {
        parent::__construct($auth);	
    }
	
    public function logout()
    { 
		auth('api')->logout();
		
        return [
          'code' => 0,
          'msg' => "退出成功",
          'data' => [],
          'timestamp' => time()
        ];
	}
    /**
     * 用帐号密码登录
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    { 
        $data = request(['username', 'password']);

		$data = ["name"=>$request->username, 'password'=>$request->password];

		$token =$this->getRepositories()->login($data,$request);
		
        $data = [
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => auth()->factory()->getTTL() * 60
		];

        return $this->success($data);
    }	

}
