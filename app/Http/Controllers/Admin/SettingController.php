<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Config;
use App\Repositories\Config as repo;
use App\Http\Requests\ConfigRequest;

class SettingController extends Controller
{
    public function __construct(repo $repo)
    {
        parent::__construct($repo);	
    }
	
    /**
     * 显示配置
     *
     * @return \Illuminate\Http\Response
     */
    public function config(Request $request)
    {
		return $this->success($this->getRepositories()->show(),"获取配置成功");
	}
    /**
     * 保存配置文件中的图片
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
		$request->validate([
			'file' => 'required|mimes:jpeg,gif,png|dimensions:min_width=200,min_height=200,max_width=600,max_height=600,ratio=1'
		],[
			'file.required'=>"文件必须上传",
			'file.mimes'=>"图片格式必为jpg,png,gif",
			'file.dimensions'=>"图片大小为宽高比为1:1,最小为200，最大为600",
			'file.file'=>"必须为图片",
		]);
		
		$avatar=$this->getRepositories()->upload($request,['form'=>['user'=>$request->user]]);
		
		return $this->success($avatar,"上传配置图片成功");
    }
	
    /**
     * 保存配置文件
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ConfigRequest $request)
    {
	   $this->getRepositories()->store($request->all(),['form'=>['user'=>$request->user]]);

	   return $this->success([],"保存配置完成");
    }

}
