<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Repositories\Profile;
use App\Model\Notice;
use App\Http\Resources\UserResource;
use App\Http\Resources\NoticeResource;
use App\Http\Resources\NoticeCollection;
use Illuminate\Validation\ValidationException;



class ProfileController extends Controller
{
    public function __construct(Profile $profileRepositories,Request $request)
    {
		$profileRepositories->setUser($request->user('api'));
		
        parent::__construct($profileRepositories);	
    }
    /**
     * 显示个人信息
     *
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
		return $this->success(new UserResource($request->user('api')),"获取个人信息成功");
    }
    /**
     * 修改个人信息
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
	    $this->getRepositories()->store($request->all(),['form'=>['user'=>$request->user]]);
		
        return $this->success([],"修改个人信息成功");
    }
    /**
     * 显示登录用户的信息
     *
     * @return \Illuminate\Http\Response
     */
    public function avatar(Request $request)
    {
		$request->validate([
			'file' => 'required|mimes:jpeg,gif,png|dimensions:min_width=200,min_height=200,max_width=600,max_height=600,ratio=1'
		],[
			'file.required'=>"文件必须上传",
			'file.mimes'=>"图片格式必为jpg,png,gif",
			'file.dimensions'=>"图片大小为宽高比为1:1,最小为200，最大为600",
			'file.file'=>"必须为图片",
		]);
		
		$avatar=$this->getRepositories()->avatar($request,['form'=>['user'=>$request->user]]);
		
		return $this->success($avatar,"头像上传完成");
    }
    /**
     * 修改密码
     *
     * @return \Illuminate\Http\Response
     */
    public function password(PasswordRequest $request)
    {
	
	    $this->getRepositories()->password($request->all(),['form'=>['user'=>$request->user]]);
		
		return $this->success('密码修改成功');
    }
    /**
     * 获取用户菜单
     *
     * @return \Illuminate\Http\Response
     */
    public function menu(Request $request)
    {
		return $this->success($request->user('api')->menus(),"菜单获取成功");
    }
    /**
     * 获取用户通知
     *
     * @return \Illuminate\Http\Response
     */
    public function notice(Request $request)
    {

		$limit=$request->limit?$request->limit:10;
		
		$key=trim($request->get("key",''));
		
		$read=$request->get("read",-1);
		
		$type=$request->get("type",-1);
		
		$notice=Notice::withTrashed()->where("user_id",$request->user("api")->id);
		
		if(isset($key)){
			$notice=$notice->where('title','like','%'.$key.'%');
		}
		
		if(isset($read)&&$read!=-1){$notice=$notice->where('is_read',$read);}
		
		if(isset($type)&&$type!=-1){$notice=$notice->where('type',$type);}
		
		return $this->success(new NoticeCollection($notice->paginate($limit)));
    }
    /**
     * 设置通知已读
     *
     * @return \Illuminate\Http\Response
     */
    public function read(Request $request)
    {
		$this->getRepositories()->read($request->all(),['form'=>['user'=>$request->user]]);
		 
		return $this->success("设置通知为已读");
    }
	
    /**
     * 删除通知
     *
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
		$this->getRepositories()->remove($request->all(),['form'=>['user'=>$request->user]]);
		 
		return $this->success("删除通知成功");
    }
    /**
     * 恢复通知
     *
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
		$this->getRepositories()->restore($request->all(),['form'=>['user'=>$request->user]]);
		 
		return $this->success("恢复通知");
    }
	
    /**
     * 显示通知详情
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
		return $this->success(Notice::where("id",$id)->where("user_id",$request->user("api")->id)->first());
    }
	
    /**
     * 统计用户没有读的消息条数
     *
     * @return \Illuminate\Http\Response
     */
    public function unread(Request $request)
    {
		return $this->success(Notice::where("user_id",$request->user("api")->id)->where("is_read",0)->count());
    }
}
