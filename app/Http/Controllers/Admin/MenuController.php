<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MenuRequest;
use App\Model\Menu;
use App\Repositories\Menu as objMenu;
use App\Http\Resources\MenuResource;
use App\Http\Resources\MenuCollection;

class MenuController extends Controller
{
	
    public function __construct(objMenu $categoryRepositories)
    {
        parent::__construct($categoryRepositories);	
    }
    /**
     * 获取所有菜单
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
		return $this->success(new MenuCollection(Menu::get()));
    }
    /**
     * 保存用户修改或添加的菜单
     *
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
		$data=$request->all();
		
		$this->getRepositories()->store($data,['form'=>['user'=>$request->user]]);
		
		return $this->success([],'菜单保存成功');
		
    }	
    /**
     * 删除菜单
     *
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
		$this->getRepositories()->remove($request->all(),['form'=>['user'=>$request->user]]);
		
		return $this->success([],'删除菜单成功');
    }
}
