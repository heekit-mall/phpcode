<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Role;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleResource;
use App\Http\Resources\RoleCollection;
use App\Repositories\Role as objRole;


class RoleController extends Controller
{

    public function __construct(objRole $categoryRepositories)
    {
        parent::__construct($categoryRepositories);	
    }
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
		$limit=$request->limit?$request->limit:10;
		
		return $this->success(new RoleCollection(Role::paginate($limit)));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
	   $this->getRepositories()->store($request->all(),['form'=>['user'=>$request->user]]);

	   return $this->success([],'保存角色成功');
    }

	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
	   $this->getRepositories()->remove($request->all(),['form'=>['user'=>$request->user]]);
	   
	   return $this->success([],'删除角色成功');
    }
}
