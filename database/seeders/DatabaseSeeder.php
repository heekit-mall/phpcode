<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'admin',  
			'password' => Hash::make('admin888'),			
            'nickname' => '管理员',   
            'role_id' => -1,
            'phone' => '13800000000',
            'avatar' => '/img/nopic.jpg',
            'last_at' => '2019-05-20',
            'last_ip' => '127.0.0.1',
            'login_times' => '0',
            'passed' => 1,
            'desc' => '系统用户',
			'is_system' => '1'
        ]]);
		
		/*权限模块*/
        DB::table('accesses')->insert([[
			'id' => '1',
			'parent_id' => '0',
            'name' => '用户管理',   
            'code' => 'USER_GET', 
            'path' => 'api/member/user',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '2',
			'parent_id' => '1',
            'name' => '添加用户',  
            'code' => 'USER_ADD', 
            'path' => 'api/member/user',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '3',
			'parent_id' => '1',
            'name' => '修改用户', 
            'code' => 'USER_EDIT', 
            'path' => 'api/member/user',
            'method' => 'PUT',
            'desc' => ''
        ],[
			'id' => '4',
			'parent_id' => '1',
            'name' => '删除用户', 
            'code' => 'USER_REMOVE', 
            'path' => 'api/member/user',
            'method' => 'DELETE',
            'desc' => ''
        ],[
			'id' => '5',
			'parent_id' => '0',
            'name' => '角色管理', 
            'code' => 'ROLE_GET', 
            'path' => 'api/member/role',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '6',
			'parent_id' => '5',
            'name' => '添加角色', 
            'code' => 'ROLE_ADD', 
            'path' => 'api/member/role',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '7',
			'parent_id' => '5',
            'name' => '修改角色',  
            'code' => 'ROLE_EDIT', 
            'path' => 'api/member/role',
            'method' => 'PUT',
            'desc' => ''
        ],[
			'id' => '8',
			'parent_id' => '5',
            'name' => '删除角色',   
            'code' => 'ROLE_REMOVE', 
            'path' => 'api/member/role',
            'method' => 'DELETE',
            'desc' => ''
        ],[
			'id' => '9',
			'parent_id' => '0',
            'name' => '菜单管理',  
            'code' => 'MENU_GET', 
            'path' => 'api/setting/menus',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '10',
			'parent_id' => '9',
            'name' => '添加菜单',  
            'code' => 'MENU_ADD', 
            'path' => 'api/setting/menus',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '11',
			'parent_id' => '9',
            'name' => '修改菜单',  
            'code' => 'MENU_EDIT', 
            'path' => 'api/setting/menus',
            'method' => 'PUT',
            'desc' => ''
        ],[
			'id' => '12',
			'parent_id' => '9',
            'name' => '删除菜单',  
            'code' => 'MENU_REMOVE', 
            'path' => 'api/setting/menus',
            'method' => 'DELETE',
            'desc' => ''
        ],[
			'id' => '13',
			'parent_id' => '0',
            'name' => '所有权限',   
            'code' => 'ACCESS_GET', 
            'path' => 'api/setting/access',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '14',
			'parent_id' => '13',
            'name' => '添加权限',   
            'code' => 'ACCESS_ADD', 
            'path' => 'api/setting/access',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '15',
			'parent_id' => '13',
            'name' => '修改权限',   
            'code' => 'ACCESS_EDIT', 
            'path' => 'api/setting/config',
            'method' => 'PUT',
            'desc' => ''
        ],[
			'id' => '16',
			'parent_id' => '13',
            'name' => '删除权限',   
            'code' => 'ACCESS_REMOVE', 
            'path' => 'api/setting/config',
            'method' => 'DELETE',
            'desc' => ''
        ],[
			'id' => '17',
			'parent_id' => '13',
            'name' => '查看链接',   
            'code' => 'ACCESS_URI', 
            'path' => 'api/setting/uris',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '18',
			'parent_id' => '0',
            'name' => '获取配置',   
            'code' => 'CONFIG_GET', 
            'path' => 'api/setting/config',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '19',
			'parent_id' => '18',
            'name' => '保存配置',   
            'code' => 'CONFIG_STORE', 
            'path' => 'api/setting/config',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '20',
			'parent_id' => '18',
            'name' => '上传图片',   
            'code' => 'CONFIG_UPLOAD', 
            'path' => 'api/setting/config/upload',
            'method' => 'POST',
            'desc' => ''
        ],[
			'id' => '21',
			'parent_id' => '0',
            'name' => '运行日志',   
            'code' => 'LOGCODE_GET', 
            'path' => 'api/system/logger/code',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '22',
			'parent_id' => '21',
            'name' => '日志详情',   
            'code' => 'LOGCODE_DETAIL', 
            'path' => 'api/system/logger/code/{date?}',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '23',
			'parent_id' => '0',
            'name' => '前端日志',   
            'code' => 'LOG_GET', 
            'path' => 'api/system/logger/api',
            'method' => 'GET|HEAD',
            'desc' => ''
        ],[
			'id' => '24',
			'parent_id' => '23',
            'name' => '保存前端',   
            'code' => 'LOG_STORE', 
            'path' => 'api/system/logger/api',
            'method' => 'POST',
            'desc' => ''
        ]]);
		
		/*菜单管理*/
        DB::table('menus')->insert([[
			'id' => '1',
			'parent_id' => '0',			
			'name' => '权限管理',
            'prefix' => '/', 
            'path' => '',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '2',
			'parent_id' => '1',			
			'name' => '系统用户',
            'prefix' => '/', 
            'path' => 'member/user',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '3',
			'parent_id' => '1',			
			'name' => '角色信息',
            'prefix' => '/', 
            'path' => 'member/role',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '4',
			'parent_id' => '0',			
			'name' => '系统设置',
            'prefix' => '/', 
            'path' => '',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '5',
			'parent_id' => '4',			
			'name' => '站点配置',
            'prefix' => '/', 
            'path' => 'setting/config',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '6',
			'parent_id' => '4',			
			'name' => '菜单资源',
            'prefix' => '/', 
            'path' => 'setting/menus/index',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '7',
			'parent_id' => '4',			
			'name' => '访问权限',
            'prefix' => '/', 
            'path' => 'setting/access/index',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '8',
			'parent_id' => '0',			
			'name' => '系统信息',
            'prefix' => '/', 
            'path' => 'setting/access/index',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '9',
			'parent_id' => '8',			
			'name' => '运行日志',
            'prefix' => '/', 
            'path' => 'setting/access/index',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ],[
			'id' => '10',
			'parent_id' => '8',			
			'name' => '前端日志',
            'prefix' => '/', 
            'path' => 'setting/access/index',
            'icon' => '',   
            'target' => '_self', 
            'is_system' => '1'
        ]]);
		//菜单权限
		DB::table('access_menu')->insert([[
			'menu_id' => '2',			
			'access_id' => '1'
        ],[
			'menu_id' => '2',			
			'access_id' => '2'
        ],[
			'menu_id' => '2',			
			'access_id' => '3'
        ],[
			'menu_id' => '2',			
			'access_id' => '4'
        ],[
			'menu_id' => '3',			
			'access_id' => '5'
        ],[
			'menu_id' => '3',			
			'access_id' => '6'
        ],[
			'menu_id' => '3',			
			'access_id' => '7'
        ],[
			'menu_id' => '3',			
			'access_id' => '8'
        ],[
			'menu_id' => '5',			
			'access_id' => '18'
        ],[
			'menu_id' => '5',			
			'access_id' => '19'
        ],[
			'menu_id' => '5',			
			'access_id' => '20'
        ],[
			'menu_id' => '6',			
			'access_id' => '9'
        ],[
			'menu_id' => '6',			
			'access_id' => '10'
        ],[
			'menu_id' => '6',			
			'access_id' => '11'
        ],[
			'menu_id' => '6',			
			'access_id' => '12'
        ],[
			'menu_id' => '7',			
			'access_id' => '13'
        ],[
			'menu_id' => '7',			
			'access_id' => '14'
        ],[
			'menu_id' => '7',			
			'access_id' => '15'
        ],[
			'menu_id' => '7',			
			'access_id' => '16'
        ],[
			'menu_id' => '7',			
			'access_id' => '17'
        ],[
			'menu_id' => '9',			
			'access_id' => '21'
        ],[
			'menu_id' => '9',			
			'access_id' => '22'
        ],[
			'menu_id' => '10',			
			'access_id' => '23'
        ],[
			'menu_id' => '10',			
			'access_id' => '24'
        ]]);
		
		/*配置模块*/
        DB::table('configs')->insert([[		
            'name' => '网站名', 
            'key' => 'app.name', 
            'value' => 'heekit商城', 
            'default_value' => 'heekit商城', 
            'option_value' => '', 
            'help' => '请输入站点名', 
            'field_type' => 'text',
            'sort' => 0,
            'is_public' => 0, 
			'group_label' => '系统',
			'group_name' => 'system'
		],[		
            'name' => 'DEBUG', 
            'key' => 'app.debug', 
            'value' => '0', 
            'default_value' => '0', 
            'option_value' => '', 
            'help' => '', 
            'field_type' => 'switch',
            'sort' => 1,
            'is_public' => 0, 
			'group_label' => '系统',
			'group_name' => 'system'
		],[		
            'name' => '网站地址', 
            'key' => 'app.url', 
            'value' => '', 
            'default_value' => 'http://www.heekit.com', 
            'option_value' => '', 
            'help' => '', 
            'field_type' => 'text',
            'sort' => 2,
            'is_public' => 0, 
			'group_label' => '系统',
			'group_name' => 'system'
		],[		
            'name' => '网站logo', 
            'key' => 'app.logo', 
            'value' => '', 
            'default_value' => '', 
            'option_value' => '', 
            'help' => '', 
            'field_type' => 'image',
            'sort' => 3,
            'is_public' => 0, 
			'group_label' => '系统',
			'group_name' => 'system'
		],[		
            'name' => '关于我们', 
            'key' => 'app.desc', 
            'value' => '', 
            'default_value' => '', 
            'option_value' => '', 
            'help' => '', 
            'field_type' => 'longtext',
            'sort' => 3,
            'is_public' => 0, 
			'group_label' => '系统',
			'group_name' => 'system'
		]]);
    }
}
